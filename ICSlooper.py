from icalendar import Calendar
import sys
import errno
import pendulum
import os
import datetime

TEST=False

OPTIONS=['ics', 'from-date', 'html']

HTML_PREFIX='''<table>
'''

HTML_SUFFIX='''</table>
'''

def err(message):
    sys.stderr.write(str(message)+'\n')

def before(text, subtext):
    offset=text.find(subtext)
    if offset<0:
        return None # before
    return text[:offset] # before

def after(text, subtext):
    offset=text.find(subtext)
    if offset<0:
        return None # after
    return text[offset+len(subtext):] # after

def dequote(text):
     if text is None or \
        len(text)<2 or \
        not (text[0] in ["'", '"'] and \
             text[0]==text[-1]
            ):
        return text
     return text[1:-1] # dequote

def get_options(prefix='--', all_options=None):
    options={}
    for arg in sys.argv[1:]:
        if arg.startswith(prefix):
            arg=arg[len(prefix):]
            key=before(arg, '=')
            if (not (all_options is None)) and not (key in all_options):
               err ('Unknown option: '+key)
               return None # get_options
           
            value=after(arg, '=')
            options[key]=dequote(value)
    return options # get_options

options=get_options(all_options=OPTIONS)

def parse_date(datetime_string):
    try:
        return pendulum.parse(datetime_string, strict=False)
    except:
        err ('Invalid datetime: '+datetime_string)
        exit (errno.EINVAL) # Invalid argument

def hh_mm(elapsed):
    elapsed_str=str(elapsed)
    str_split=elapsed_str.split(':')
    hh=str_split[0]
    mm=str_split[1]
    return hh+':'+mm
#===========================MAIN CODE========================
        
if options is None:
    exit (errno.EIVAL) # Invalid argument...

#print(options)

if TEST:
    if 'test' in options:
        print(str(options['test']))
    else:
        err('--test option not given...')

if (not ('ics' in options)) or options['ics'] is None:
   err('missing --ics option...')
   exit (errno.EINVAL) # Invalid argument...
   
if not os.path.isfile(options['ics']):
    err("File '"+options['ics']+'" does not exist...')
    exit (errno.EINVAL)
        
with open(options['ics'], 'rb') as calendar_file:
    calendar=Calendar.from_ical(calendar_file.read())
    
if 'from-date' in options:
   from_date=options['from-date']
   if not (from_date is None):
      from_date=parse_date(from_date)
      print(from_date)

else: # not ('from-date' in options)
   from_date=None

if 'html' in options:
    if options['html'] is None:
        err('html file not specified...')
        exit(errno.EINVAL)
    if not options['html'].endswith('.html'):
        options['html']+='.html'
        html=open(options['html'],'w')
else: # not ('html' in options)
    html=sys.stdout

html.write(HTML_PREFIX)

for event in calendar.walk('VEVENT'):
    dtstart=event.decoded('dtstart')
    if from_date is None or dtstart>=from_date:
        dtend=event.decoded('dtend')
        duration=dtend-dtstart
        duration_str=hh_mm(duration)
        summary=event['SUMMARY']
        #print(dtstart.strftime('%m/%d/%Y %I:%M %p') + " "+ duration_str)
        html.write('<tr>\n'
                   '<td>'+
                   dtstart.strftime('%m/%d/%Y %I:%M %p')+
                   '</td>\n'+
                   '<td>'+
                   duration_str+
                   '</td>\n'+
                   '<td>'+
                   summary+
                   '</td>\n'+
                   '</tr>\n'
                  )

html.write(HTML_SUFFIX)
html.close()
                   
