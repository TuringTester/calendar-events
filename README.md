# Calendar Events

## Test and Deploy

Organizing Calendar Events for a newsletter or other purposes

This project will help you if you need to create a newsletter or just need to consolidate all information regarding up coming events. 

The project was put together using Mozilla Thunderbird's calendar application and python. Two test events were made in the calendar app, which were then exported as an ".ics" file and saved in the same directory as the python program.

##USAGE
{name of module}.py --ics={title of the file you just exported from Thunderbird} --from-date={the date you want to get events from and after} --html={the name of the to-be-created '.html' file (not including the '.html' suffix)}

Doing the above will generate a ".html" file in your current directory.

For help, send a message at www.legocoder.com/contact/

Future releases of this app will also check for a type in the console command.

